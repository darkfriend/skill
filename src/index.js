import express from 'express';
import cors from 'cors';

const app = express();
app.use(cors());
app.get('/', (req, res) => {
  res.json({
    hello: 'JS World',
  });
});

app.get('/task2A', (req, res) => {
	var a = +req.query.a || 0;
	var b = +req.query.b || 0;
	var sum = a+b;
  	res.send(sum.toString());
});

app.get('/task2B', (req, res) => {
	var fio = req.query.fullname || '';
	var shortFio = '';
	if(fio && !/([0-9_\/]+)/.test(fio)){
		var arFio = fio.trim().split(/\s+/);
		if(arFio.length>3){
			shortFio = 'Invalid fullname';
		} else {
			shortFio = arFio.pop().toLowerCase();
			shortFio = shortFio.charAt(0).toUpperCase() + shortFio.substr(1);
			arFio.forEach((item)=>{
				shortFio += ' '+item.charAt(0).toUpperCase()+'.';
			});
		}
	} else {
		shortFio = 'Invalid fullname';
	}
  	res.send(shortFio.toString());
});

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
